### Usage

```
npm install
npm start  or npm start DEBUG=true 
open http://localhost:3000
```

If you are on windows, run `set DEBUG=true && npm start` instead.

### Dependencies

* React
* Webpack
* [webpack-dev-server](https://github.com/webpack/webpack-dev-server)
* [babel-loader](https://github.com/babel/babel-loader)
* [react-hot-loader](https://github.com/gaearon/react-hot-loader)

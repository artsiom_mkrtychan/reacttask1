import * as types from '../constants/ActionTypes';
import omit from 'lodash/object/omit';
import assign from 'lodash/object/assign';
import mapValues from 'lodash/object/mapValues';

const initialState = {
    showAddModal: false,
    friends: [1, 2, 3, 4],
    friendsById: {
        1: {
            id: 1,
            name: 'Theodore',
            lastName: 'Roosevelt',
            jobPosition: 'President',
            photo: 'http://www.studentland.ua/img/events/dc_sem/flat-faces-icons-circle-16.png',
            shown: true
        },
        2: {
            id: 2,
            name: 'Abraham',
            lastName: 'Lincoln',
            jobPosition: 'CEO',
            photo: 'https://arawal.files.wordpress.com/2015/07/flat-faces-icons-circle-3.png',
            shown: true
        },
        3: {
            id: 3,
            name: 'George',
            lastName: 'Washington',
            jobPosition: 'CFO',
            photo: 'http://www.iconsfind.com/wp-content/uploads/2016/10/20161014_58006bf27a2d0.png',
            shown: true
        },
        4: {
            id: 4,
            name: 'Theo',
            lastName: 'Roosevelt2',
            jobPosition: 'President',
            photo: 'http://www.studentland.ua/img/events/dc_sem/flat-faces-icons-circle-16.png',
            shown: true
        }
    }
};

export default function friends(state = initialState, action) {
    switch (action.type) {
       
        case types.ADD_FRIEND_FROM_FORM:
            const newId = state.friends[state.friends.length - 1] + 1;
            console.log("ADD_FRIEND_FROM_FORM reducer")
            return {
                ...state,
                friends: state.friends.concat(newId),
                friendsById: {
                    ...state.friendsById,
                    [newId]: {
                        id: newId,                        
                        name: action.name,
                        lastName: action.lastName,
                        jobPosition: action.jobPosition,
                        photo: action.photo,
                        shown: true                        
                    }
                }
            };        

        case types.DELETE_FRIEND:
            return {
                ...state,
                friends: state.friends.filter(id => id !== action.id),
                friendsById: omit(state.friendsById, action.id)
            };
   

        case types.SEARCH_FRIEND:
            return {
                ...state,
                friendsById: mapValues(state.friendsById, (friend) => {
                    let friendName = friend.name.toLowerCase();
                    let actionName = action.name.toLowerCase();                   
                
                    return assign({}, friend, {
                        shown: friendName.includes(actionName)
                    })
                })
            };
         default:
            return state;
    }
}

import * as types from '../constants/ActionTypes';

export function addFriendFromForm(name,lastName,jobPosition,photo) {
  return {
    type: types.ADD_FRIEND_FROM_FORM,
    name,
    lastName,
    jobPosition,
    photo
  };
}

export function deleteFriend(id) {
  return {
    type: types.DELETE_FRIEND,
    id
  };
}

export function searchFriend(name) {
  return {
    type: types.SEARCH_FRIEND,
    name
  };
}

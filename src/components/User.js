import React, { Component, PropTypes } from 'react'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import classnames from 'classnames';
import { Link } from 'react-router'
import styles from './User.css';

class User extends Component {

  render() {
    let userIndex = this.props.params.userId
    let user = this.props.friendsById[userIndex]

    return (
      <div className={`panel panel-primary`}>
        <div className={`panel-heading`}>
          <h3 className={`panel-title`}>Person info</h3>
        </div>
        <div className={`panel-body`}>
          <div className={styles.infoWrapper}>
            <div className={styles.photoWrapper}>
              <img src={user.photo} className={styles.friendPicture}/>
            </div>
            <div className={styles.specsWrapper}>
              <div className={`list-group`}>
                <div className={`list-group-item`}><span>{user.name} {user.lastName}</span></div>
                <div className={`list-group-item`}>{user.jobPosition}</div>
              </div>
            </div>
          </div>
          <button className={`btn btn-default`}><Link to='/'>Return</Link></button>
        </div>
      </div>
    )
  }
}

function mapStateToProps (store) {
  return {
    friendsById: store.friendlist.friendsById
  }
}

export default connect(mapStateToProps)(User)
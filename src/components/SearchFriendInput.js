import React, { Component, PropTypes } from 'react';
import classnames from 'classnames';
import styles from './SearchFriendInput.css';

export default class SearchFriendInput extends Component {
  static propTypes = {
    searchFriend: PropTypes.func.isRequired
  };

  render () {
    return (
      <input
        name = "search-input"
        type="text"
        autoFocus="true"
        className={classnames('form-control', styles.searchFriendInput)}
        placeholder="Search by name"
        value={this.state.name}
        onChange={this.handleChange.bind(this)} />       
    );
  }

  constructor (props, context) {
    super(props, context);
    this.state = {
      name: this.props.name || '',
    };   
  }

  handleChange (e) {
    const name = e.target.value.trim();  
      
    this.setState({ name: e.target.value });
    this.props.searchFriend(name);      
  } 

}

import React, { Component, PropTypes } from 'react';
import { Form, FormGroup, FormControl, Button } from 'react-bootstrap';
import classnames from 'classnames';

export default class AddFriendForm extends Component {
  static propTypes = {
    addFriendFromForm: PropTypes.func.isRequired
  };

  render () {     
    return (
        <div>
            <button 
                className={classnames('btn', 'btn-success')} data-toggle="modal" data-target="#myModalForm">
              Add employee 
            </button>

            <div className={classnames('modal', 'fade')} id="myModalForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div className={classnames('modal-dialog')} role="document">
                <div className={classnames('modal-content')}>
                  <div className={classnames('modal-header')}>
                    <button type="button" className={classnames('close')} data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 className={classnames('modal-title')} id="myModalLabel">Modal title</h4>
                  </div>
                  <div className={classnames('modal-body')}>
                        <div className={classnames('form-group')}>                            
                            <input 
                              className={classnames('form-control')} 
                              id="name" 
                              name="name" 
                              ref="name" 
                              type="text" 
                              placeholder="Input first name"  
                              value={this.state.name}           
                              onChange={this.handleChange.bind(this)}/>  
                        </div>
                        <div className={classnames('form-group')}>  
                            <input 
                              className={classnames('form-control')} 
                              name="lastName" 
                              ref="lastName" 
                              type="text" 
                              placeholder="Input last name" 
                              value={this.state.lastName}           
                              onChange={this.handleChange.bind(this)}/>
                        </div>
                        <div className={classnames('form-group')}>  
                        <input 
                          className={classnames('form-control')} 
                          name="jobPosition" 
                          ref="jobPosition" 
                          type="text" 
                          placeholder="Input job position"
                          value={this.state.jobPosition}           
                          onChange={this.handleChange.bind(this)}/>
                        </div>
                        <div className={classnames('form-group')}>    
                        <input 
                          className={classnames('form-control')} 
                          name="photo"
                          ref="photo"
                          type="text" 
                          placeholder="Input photo url"
                          value={this.state.photo}           
                          onChange={this.handleChange.bind(this)}/>
                        </div>
                  </div>
                  <div className={classnames('modal-footer')}>
                    <button 
                      type="button" 
                      className={classnames('btn', 'btn-default')} 
                      onClick={this.handleDiscart.bind(this)}
                      data-dismiss="modal">
                        Close
                    </button>
                    <button 
                      type="button" 
                      onClick={this.handleSave.bind(this)}
                      className={classnames('btn', 'btn-primary')}
                      data-dismiss="modal">
                        Save changes
                    </button>
                  </div>
                </div>
              </div>
            </div>
        </div>

    );
  }

  constructor (props, context) {
    super(props, context);
      
    this.state = {
      name: this.props.name || '',
      lastName: this.props.lastName || '',
      jobPosition: this.props.jobPosition || '',
      photo: this.props.photo || ''
    };    
  } 

    handleChange(e){
        switch (e.target.name) {
            case "name":
               this.setState({ name: e.target.value });   
               break;
            case "lastName":
               this.setState({ lastName: e.target.value });   
               break;
            case "jobPosition":
               this.setState({ jobPosition: e.target.value });  
               break;
            case "photo":
               this.setState({ photo: e.target.value });     
               break;
        }         
    }
    
    handleDiscart(e){
        this.setState({
            name: '', 
            lastName: '', 
            jobPosition: '', 
            photo: '', 
        });
    }

   

  handleSave (e) {
      
    e.preventDefault();    
      
    const name = this.state.name.trim();
    const lastName = this.state.lastName.trim();
    const jobPosition = this.state.jobPosition.trim();
    const photo = this.state.photo.trim();
    
    if(name !== '' || lastName !== '' || jobPosition !== '' || photo !== ''){
        this.props.addFriendFromForm(name,lastName,jobPosition,photo);
    }     
    
    this.setState({
        name: '', 
        lastName: '', 
        jobPosition: '', 
        photo: '', 
    });
    
  }

}

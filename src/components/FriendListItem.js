import React, {Component, PropTypes} from 'react';
import { Link } from 'react-router';
import classnames from 'classnames';
import styles from './FriendListItem.css';

export default class FriendListItem extends Component {   

    static propTypes = {
        id: PropTypes.number.isRequired,
        name: PropTypes.string.isRequired,
        lastName: PropTypes.string.isRequired,
        jobPosition: PropTypes.string.isRequired,
        photo: PropTypes.string.isRequired,
        shown: PropTypes.bool,        
        deleteFriend: PropTypes.func.isRequired       
    };

    render() {     
        
        return (
              this.props.shown ?
                <li className={styles.friendListItem} >
                    <div className={styles.friendInfos}>
                            <img src={this.props.photo} className={styles.friendPicture}/>

                            <div className={styles.friendInfosWrap}>
                                <div>
                                    <Link to={'/users/' + this.props.id}><span>{this.props.name} {this.props.lastName}</span></Link>
                                </div>
                                <div>
                                    <small className={styles.friendPosition}>{this.props.jobPosition}</small>
                                </div>
                            </div>
                    </div>
                    <div className={styles.friendActions}>                        
                        <button className={`btn btn-default ${styles.btnAction}`}
                                onClick={() => this.props.deleteFriend(this.props.id)}>
                            <i className="fa fa-trash"/>
                        </button>
                    </div>
                </li>
             : null
        );
    }
    
}

import React, { Component, PropTypes } from 'react';
import styles from './FriendListApp.css';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Button } from 'react-bootstrap';
import * as FriendsActions from '../actions/FriendsActions';
import { FriendList, SearchFriendInput, AddFriendForm} from '../components';

@connect(state => ({
  friendlist: state.friendlist
}))
export default class FriendListApp extends Component {

  static propTypes = {
    friendsById: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired
  };

  render () {
    const { friendlist: { friendsById }, dispatch } = this.props;
    const actions = bindActionCreators(FriendsActions, dispatch);

    return (
      <div className={styles.friendListApp}>        
        <h1>Employee directory</h1>        
        <div className={styles.friendListAppActions}>
            <div className={styles.searchInput}>
                <SearchFriendInput searchFriend={actions.searchFriend}/>
            </div>
            <div className={styles.addButton}>                  
                <AddFriendForm addFriendFromForm={actions.addFriendFromForm}/>
            </div>
        </div>    
        <FriendList friends={friendsById} actions={actions} />  
      </div>
    );
  }
}

import React, { Component } from 'react';
import { combineReducers } from 'redux';
import { Provider } from 'react-redux';
import { Router, Route, browserHistory } from 'react-router';
import { syncHistoryWithStore, routerReducer } from 'react-router-redux'

import { createStore, renderDevTools } from '../store_enhancers/devTools';
import { deleteFriend, searchFriend } from '../actions/FriendsActions';

import FriendListApp from './FriendListApp';
import { FriendList, SearchFriendInput, AddFriendForm, NotFound, User} from '../components';
import * as reducers from '../reducers';

const reducer = combineReducers({
    ...reducers,
    routing: routerReducer});

const store = createStore(reducer);

const history = syncHistoryWithStore(browserHistory, store);

export default class App extends Component {
  render() {
    return (
        <div>
            <Provider store={store}>  
              {() =>
                  <Router history={history}>
                     <Route path="/" component={FriendListApp}/>
                     <Route path="/users/:userId" component={User}/>
                     <Route path="/*" component={NotFound}/>
                  </Router>
              }
            </Provider>     
        </div>
    );
  }
}
